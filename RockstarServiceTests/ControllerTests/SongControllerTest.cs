using rockstarService;
using Xunit;
using rockstarService.Domain;
using Microsoft.EntityFrameworkCore;
using System.Linq;
using rockstarService.Domain.Repositories;
using System.Collections.Generic;

namespace rockstarServiceTests
{
    public class SongControllerTest
    {

        List<Song> songs;
        
        SongBuilder modelBuilder;

        public SongControllerTest()
        {
            modelBuilder = new SongBuilder();
            songs = modelBuilder.getSongs();
    
        }

        [Fact]
        public void TestGetSongById()
        {
            var options = new DbContextOptionsBuilder<ArtistsContext>()
               .UseInMemoryDatabase(databaseName: "TestGetSongById").Options;
            var ArtistsContext = new ArtistsContext(options);
            var repository = new SongRepository(ArtistsContext);
            var songController = new SongController(repository);

            ArtistsContext.Add(songs[0]);
            ArtistsContext.SaveChanges();
            var Song = songController.GetById(1);

            Assert.Equal(1, Song.Id);
        }
           [Fact]
        public void TestGetSongByGenre()
        {
            var options = new DbContextOptionsBuilder<ArtistsContext>()
               .UseInMemoryDatabase(databaseName: "TestGetSongByGenre").Options;
            var ArtistsContext = new ArtistsContext(options);
            var repository = new SongRepository(ArtistsContext);
            var songController = new SongController(repository);

            ArtistsContext.Add(songs[0]);
            ArtistsContext.Add(songs[1]);
            ArtistsContext.Add(songs[2]);
            ArtistsContext.Add(songs[3]);   
            ArtistsContext.SaveChanges();
            var Songs = songController.GetByGenre("Metal");

            Assert.Equal(2, Songs.Count());
        }
  
        [Fact]
        public void TestSongNotFound()
        {
            var Options = new DbContextOptionsBuilder<ArtistsContext>()
               .UseInMemoryDatabase(databaseName: "TestSongNotFound").Options;
            var ArtistsContext = new ArtistsContext(Options);
            var repository = new SongRepository(ArtistsContext);
            var songController = new SongController(repository);

            var song = songController.GetById(1);

            Assert.Null(song);
        }

        [Fact]
        public void TestCreateSong()
        {
            var Options = new DbContextOptionsBuilder<ArtistsContext>()
               .UseInMemoryDatabase(databaseName: "TestCreateSong").Options;
            var ArtistsContext = new ArtistsContext(Options);
            var repository = new SongRepository(ArtistsContext);
            var songController = new SongController(repository);
            songController.Create(songs[0]);

            Assert.Equal(1, ArtistsContext.Songs.Count());
            Assert.Equal(1, ArtistsContext.Songs.First().Id);
        }
   
        [Fact]
        public void TestCreateSongThatIsNotMetal()
        {
            var Options = new DbContextOptionsBuilder<ArtistsContext>()
               .UseInMemoryDatabase(databaseName: "TestCreateSongThatIsNotMetal").Options;
            var ArtistsContext = new ArtistsContext(Options);
            var repository = new SongRepository(ArtistsContext);
            var songController = new SongController(repository);
            songController.Create(songs[1]);

            Assert.Equal(0, ArtistsContext.Songs.Count());
        }
        [Fact]
        public void TestCreateSongThatIsNotOld()
        {
            var Options = new DbContextOptionsBuilder<ArtistsContext>()
               .UseInMemoryDatabase(databaseName: "TestCreateSongThatIsNotOld").Options;
            var ArtistsContext = new ArtistsContext(Options);
            var repository = new SongRepository(ArtistsContext);
            var songController = new SongController(repository);
            songController.Create(songs[3]);

            Assert.Equal(0, ArtistsContext.Songs.Count());
        }

        [Fact]
        public void TestDeleteSong()
        {
            var Options = new DbContextOptionsBuilder<ArtistsContext>()
               .UseInMemoryDatabase(databaseName: "TestDeleteSong").Options;
            var ArtistsContext = new ArtistsContext(Options);
            var repository = new SongRepository(ArtistsContext);
            var songController = new SongController(repository);

            ArtistsContext.Add(songs[0]);
            ArtistsContext.SaveChanges();
            songController.Delete(songs[0]);

            Assert.Equal(0, ArtistsContext.Songs.Count());
        }

        [Fact]
        public void TestUpdateSong()
        {
            var Options = new DbContextOptionsBuilder<ArtistsContext>()
               .UseInMemoryDatabase(databaseName: "TestUpdateSong").Options;
            var ArtistsContext = new ArtistsContext(Options);
            var repository = new SongRepository(ArtistsContext);
            var songController = new SongController(repository);

            ArtistsContext.Add(songs[0]);
            ArtistsContext.SaveChanges();
            songs[0].Name = "NewName";
            songController.Update(songs[0]);

            Assert.Equal("NewName", ArtistsContext.Songs.First().Name);
        }
    }
}