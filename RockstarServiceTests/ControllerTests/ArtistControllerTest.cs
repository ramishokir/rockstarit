using rockstarService;
using Xunit;
using rockstarService.Domain;
using Microsoft.EntityFrameworkCore;
using System.Linq;
using rockstarService.Domain.Repositories;
using System.Collections.Generic;

namespace rockstarServiceTests
{
    public class ArtistControllerTest
    {

        List<Artist> artists;
        
        ArtistBuilder modelBuilder;

        public ArtistControllerTest()
        {
            modelBuilder = new ArtistBuilder();
            artists = modelBuilder.getArtists();
    
        }

        [Fact]
        public void TestGetArtistById()
        {
            var options = new DbContextOptionsBuilder<ArtistsContext>()
               .UseInMemoryDatabase(databaseName: "TestGetArtistById").Options;
            var ArtistsContext = new ArtistsContext(options);
            var repository = new ArtistRepository(ArtistsContext);
            var artistController = new ArtistController(repository);

            ArtistsContext.Add(artists[0]);
            ArtistsContext.SaveChanges();
            var Artist = artistController.GetById(1);

            Assert.Equal(1, Artist.Id);
        }
          [Fact]
        public void TestGetArtistByName()
        {
            var options = new DbContextOptionsBuilder<ArtistsContext>()
               .UseInMemoryDatabase(databaseName: "TestGetArtistByName").Options;
            var ArtistsContext = new ArtistsContext(options);
            var repository = new ArtistRepository(ArtistsContext);
            var artistController = new ArtistController(repository);

            ArtistsContext.Add(artists[0]);
            ArtistsContext.Add(artists[1]);
            ArtistsContext.Add(artists[2]);
            ArtistsContext.Add(artists[3]);
            ArtistsContext.SaveChanges();
            var Artist = artistController.GetByName("artist1");

            Assert.Equal(1, Artist[0].Id);
        }
  
  
        [Fact]
        public void TestArtistNotFound()
        {
            var Options = new DbContextOptionsBuilder<ArtistsContext>()
               .UseInMemoryDatabase(databaseName: "TestArtistNotFound").Options;
            var ArtistsContext = new ArtistsContext(Options);
            var repository = new ArtistRepository(ArtistsContext);
            var artistController = new ArtistController(repository);

            var artist = artistController.GetById(1);

            Assert.Null(artist);
        }

        [Fact]
        public void TestCreateArtist()
        {
            var Options = new DbContextOptionsBuilder<ArtistsContext>()
               .UseInMemoryDatabase(databaseName: "TestCreateArtist").Options;
            var ArtistsContext = new ArtistsContext(Options);
            var repository = new ArtistRepository(ArtistsContext);
            var artistController = new ArtistController(repository);
            artistController.Create(artists[0]);

            Assert.Equal(1, ArtistsContext.Artists.Count());
            Assert.Equal(1, ArtistsContext.Artists.First().Id);
        }

        [Fact]
        public void TestDeleteArtist()
        {
            var Options = new DbContextOptionsBuilder<ArtistsContext>()
               .UseInMemoryDatabase(databaseName: "TestDeleteArtist").Options;
            var ArtistsContext = new ArtistsContext(Options);
            var repository = new ArtistRepository(ArtistsContext);
            var artistController = new ArtistController(repository);

            ArtistsContext.Add(artists[0]);
            ArtistsContext.SaveChanges();
            artistController.Delete(artists[0]);

            Assert.Equal(0, ArtistsContext.Artists.Count());
        }

        [Fact]
        public void TestUpdateArtist()
        {
            var Options = new DbContextOptionsBuilder<ArtistsContext>()
               .UseInMemoryDatabase(databaseName: "TestUpdateArtist").Options;
            var ArtistsContext = new ArtistsContext(Options);
            var repository = new ArtistRepository(ArtistsContext);
            var artistController = new ArtistController(repository);

            ArtistsContext.Add(artists[0]);
            ArtistsContext.SaveChanges();
            artists[0].Name = "NewName";
            artistController.Update(artists[0]);

            Assert.Equal("NewName", ArtistsContext.Artists.First().Name);
        }
    }
}