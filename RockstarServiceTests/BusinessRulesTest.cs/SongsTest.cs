using rockstarService;
using Xunit;
using rockstarService.Domain;
using Microsoft.EntityFrameworkCore;
using System.Linq;
using rockstarService.Domain.Repositories;
using System.Collections.Generic;

namespace rockstarServiceTests
{
    public class SongsTest
    {
        List<Song> songs;
        
        SongBuilder modelBuilder;

        public SongsTest()
        {
            modelBuilder = new SongBuilder();
            songs = modelBuilder.getSongs();
        }

        [Fact]
        public void TestSongIsMetal()
        {
            Assert.True(songs[0].genreContainsMetal());
            Assert.True(songs[2].genreContainsMetal());
        }
         [Fact]
        public void TestSongIsNotMetal()
        {
            Assert.False(songs[1].genreContainsMetal());
            Assert.False(songs[3].genreContainsMetal());
        }
          [Fact]
        public void TestIsSongValid()
        {
            Assert.True(songs[0].IsValidYear());
            Assert.True(songs[3].IsValidYear());
        
        }
          [Fact]
        public void TestIsSongNotValid()
        {
            Assert.False(songs[1].IsValidYear());
            Assert.False(songs[2].IsValidYear());
        }
    }
}