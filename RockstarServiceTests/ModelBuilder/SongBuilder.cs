using System.Collections.Generic;
using rockstarService;
using rockstarService.Domain;


namespace rockstarServiceTests
{
    public class SongBuilder
    {

        Song song;
        Song song2;
        Song song3;
        Song song4;

        List<Song> songs;
        public SongBuilder()
        {
            song = new Song(1,"song1",1994,"artist1","shorthand1",111,1111,"genre1metal","spotyfID1","Album1");
            song2 = new Song(2,"song2",2994,"artist2","shorthand2",222,2222,"genre2","spotyfID2","Album2");
            song3 = new Song(3,"song3",2016,"artist3","shorthand3",333,3333,"genre3Metal","spotyfID3","Album3");
            song4 = new Song(4,"song4",1994,"artist4","shorthand4",444,4444,"genre4","spotyfID4","Album4");
        }
        public List<Song> getSongs()
        {

            var songs = new List<Song>();
            songs.Add(song);
            songs.Add(song2);
            songs.Add(song3);
            songs.Add(song4);

            return songs;
        }

    }
}