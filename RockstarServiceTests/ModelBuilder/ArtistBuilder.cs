using System.Collections.Generic;
using rockstarService;
using rockstarService.Domain;


namespace rockstarServiceTests
{
    public class ArtistBuilder
    {

        Artist artist;
        Artist artist2;
        Artist artist3;
        Artist artist4;

        List<Artist> artists;
        public ArtistBuilder()
        {
            artist = new Artist(1,"artist1");
            artist2 = new Artist(2,"artist2");
            artist3 = new Artist(3,"artist3");
            artist4 = new Artist(4,"artist4");
        }
        public List<Artist> getArtists()
        {

            var artists = new List<Artist>();
            artists.Add(artist);
            artists.Add(artist2);
            artists.Add(artist3);
            artists.Add(artist4);

            return artists;
        }

    }
}