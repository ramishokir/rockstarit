﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace rockstarService.Migrations
{
    public partial class SongDomain : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "Album",
                table: "Songs",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "Artist",
                table: "Songs",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "Bpm",
                table: "Songs",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<int>(
                name: "Duration",
                table: "Songs",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<string>(
                name: "Genre",
                table: "Songs",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "Name",
                table: "Songs",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "Shortname",
                table: "Songs",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "SpotifyId",
                table: "Songs",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "Year",
                table: "Songs",
                nullable: false,
                defaultValue: 0);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Album",
                table: "Songs");

            migrationBuilder.DropColumn(
                name: "Artist",
                table: "Songs");

            migrationBuilder.DropColumn(
                name: "Bpm",
                table: "Songs");

            migrationBuilder.DropColumn(
                name: "Duration",
                table: "Songs");

            migrationBuilder.DropColumn(
                name: "Genre",
                table: "Songs");

            migrationBuilder.DropColumn(
                name: "Name",
                table: "Songs");

            migrationBuilder.DropColumn(
                name: "Shortname",
                table: "Songs");

            migrationBuilder.DropColumn(
                name: "SpotifyId",
                table: "Songs");

            migrationBuilder.DropColumn(
                name: "Year",
                table: "Songs");
        }
    }
}
