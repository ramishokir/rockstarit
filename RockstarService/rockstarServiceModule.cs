using Autofac;
using NovusLogger;
using rockstarService.Domain;
using rockstarService.Domain.Repositories;
using rockstarService.Domain.Repositories.Interfaces;

namespace rockstarService
{
    public class rockstarServiceModule : Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            builder.RegisterType<SongRepository>()
                .As<ISongRepository>()
                .InstancePerLifetimeScope();

            builder.RegisterType<ArtistRepository>()
                .As<IArtistRepository>()
                .InstancePerLifetimeScope();
                           
            builder.RegisterType<ArtistController>()
                .As<ArtistController>()
                .InstancePerLifetimeScope();
            
            builder.RegisterType<SongController>()
                .As<SongController>()
                .InstancePerLifetimeScope();
        }
    }
}