﻿using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.OpenApi.Models;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Autofac;
using Autofac.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using rockstarService.Domain;
using NovusRabbitClient.ReceivingCQRSAdapter;

namespace rockstarService
{
    public class Startup
    {
        private readonly IConfiguration _config;

        public Startup(IHostingEnvironment env)
        {
            var builder = new ConfigurationBuilder()
                .SetBasePath(env.ContentRootPath)
                .AddJsonFile("appsettings.json", optional: false, reloadOnChange: true)
                .AddJsonFile($"appsettings.{env.EnvironmentName}.json", optional: true)
                .AddEnvironmentVariables();
            _config = builder.Build();
        }
        public IServiceProvider ConfigureServices(IServiceCollection services)
        {
            services.AddCors();

            services.AddDbContext<ArtistsContext>(opt =>

                opt.UseMySql(_config.GetConnectionString("LiveConnection")));

            services.AddMvc()
                    .SetCompatibilityVersion(CompatibilityVersion.Version_3_0);

            services.AddSwaggerGen(c =>
            {
                c.SwaggerDoc("v1", new OpenApiInfo { Title = "rockstar", Version = "v1" });
            });
            services.AddSingleton<RabbitListener>();
            var container = new ContainerBuilder();

            container.Populate(services);
            container.Register(x => _config).As<IConfiguration>();
            container.RegisterModule(new rockstarServiceModule());
            return new AutofacServiceProvider(container.Build());
        }

        public void Configure(IApplicationBuilder app)
        {
            app.UseSwagger(c =>
            {
                c.PreSerializeFilters.Add(
                    (swaggerDoc, httpsReq) =>
                    httpsReq.Scheme = "https");
            });
            // Enable middleware to serve generated Swagger as a JSON endpoint.
            app.UseSwagger(c =>
            {
                c.PreSerializeFilters.Add(
                    (swaggerDoc, httpReq) =>
                    swaggerDoc.Servers = new List<OpenApiServer> { new OpenApiServer { Url = $"{httpReq.Scheme}://{httpReq.Host.Value}{_config.GetConnectionString("BaseUrl")}" } });
            });


            app.UseCors(builder =>
                builder.AllowAnyOrigin()
                    .AllowAnyHeader()
                    .AllowAnyMethod()
            );

            app.UseSwaggerUI(c =>
            {
                c.SwaggerEndpoint(_config.GetConnectionString("SwaggerEndPoint"), "rockstarService V1");
                c.RoutePrefix = string.Empty;
            });

            app.UseRouting();
            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });
        }
    }
    public static class ApplicationBuilderExtensions
    {
        private static RabbitListener Listener { get; set; }
        private static IConfiguration _config { get; set; }

        public static IApplicationBuilder UseRabbitListener(this IApplicationBuilder app, IConfiguration config)
        {
            Listener = app.ApplicationServices.GetService<RabbitListener>();

            var lifetime = app.ApplicationServices.GetService<IApplicationLifetime>();
            _config = config;

            lifetime.ApplicationStarted.Register(OnStarted);

            //press Ctrl+C to reproduce if your app runs in Kestrel as a console app
            lifetime.ApplicationStopping.Register(OnStopping);

            return app;
        }

        private static void OnStarted()
        {
         
        }

        private static void OnStopping()
        {
            Listener.Deregister();
        }
    }
}