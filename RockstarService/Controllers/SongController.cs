using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc;
using rockstarService.Domain;
using rockstarService.Domain.Repositories.Interfaces;

namespace rockstarService
{
    public class SongController
    {
        private readonly ISongRepository SongRepository;
        public SongController(ISongRepository SongRepository)
        {
            this.SongRepository = SongRepository;
        }

        public List<Song> GetAll()
        {
            return SongRepository.GetAll();
        }

        public void Create(Song Song)
        {    
            if(Song.IsValidYear() && Song.genreContainsMetal()){
                SongRepository.Insert(Song);
            }
        }

        public Song GetById(long id)
        {
            return SongRepository.GetById(id);
        }

        public List<Song> GetByName(string songName)
        {
            return SongRepository.GetByName(songName);
        }
        public List<Song> GetByGenre(string genre)
        {
            return SongRepository.GetByGenre(genre);
        }

        public void Delete(Song Song)
        {
            SongRepository.Delete(Song);
        }

        public void Update(Song Song)
        {
            SongRepository.Update(Song);
        }
    }
}