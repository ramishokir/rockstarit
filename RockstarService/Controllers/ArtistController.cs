using System.Collections.Generic;
using rockstarService.Domain;
using rockstarService.Domain.Repositories.Interfaces;
using NovusRabbitClient.IntegrationEvents.Models;
using System;


namespace rockstarService
{
    public class ArtistController
    {
        private readonly IArtistRepository ArtistRepository;
        public ArtistController(IArtistRepository ArtistRepository)
        {
            this.ArtistRepository = ArtistRepository;
        }

        public List<Artist> GetAll()
        {
            return ArtistRepository.GetAll();
        }
        public void Create(Artist Artist)
        {
            ArtistRepository.Insert(Artist);
        }

        public Artist GetById(long id)
        {
            return ArtistRepository.GetById(id);
        }

        public void Delete(Artist Artist)
        {
            ArtistRepository.Delete(Artist);
        }

        public void Update(Artist Artist)
        {
            ArtistRepository.Update(Artist);
        }
        
        public List<Artist> GetByName(string songName)
        {
            return ArtistRepository.GetByName(songName);
        }
    }
}