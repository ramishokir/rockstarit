using System;
using System.Collections.Generic;
using LessonCatalogService.Helpers;
using Microsoft.AspNetCore.Mvc;
using NovusLogger;
using rockstarService.Domain;

namespace rockstarService.Application.Adapters
{
    [Route("api/Song")]
    [ApiController]
    public class SongPwaAdapter : ControllerBase
    {
        private readonly SongController SongController;
   

        public SongPwaAdapter(SongController SongController)
        {
            this.SongController = SongController;
        }

        [HttpPost]
        public ActionResult<Song> CreateSong(Song song)
        {
            try{
                if(song.IsValidYear() && song.genreContainsMetal() && SongController.GetByName(song.Name).Count == 0){
                    SongController.Create(song);
                }      
            }
            catch (BusinessRuleException e)
            {
                return Conflict(e.Message);
            }
            catch (Exception e)
            {
                return Conflict(e.Message);
            }
            return Accepted();
        }
        [HttpGet("GetByGenre/", Name = "GetByGenre")]
        public ActionResult<List<Song>> GetByGenre(string SongName)
        {
            var songs = SongController.GetByGenre(SongName);
            if (songs == null)
            {
                return NotFound();
            }
            return songs;
        }
        
        [HttpPost("InsertSongs/", Name = "InsertSongs")]
        public ActionResult<Song> InsertSongs(List<Song> songs)
        {
            foreach (var song in songs)
            {
                if(SongController.GetByName(song.Name).Count == 0){
                    SongController.Create(song);
                }
            }
            return Accepted();
        }
        
        [HttpPut("{id}/updateSong")]
        public IActionResult updateSong(long id, Song song)
        {
            try
            {
                var existingSong = SongController.GetById(id);
                if (existingSong == null)
                {
                    return NotFound();
                }

                if (!string.IsNullOrEmpty(song.Name)) existingSong.Name = song.Name;
                if (song.Year != 0) existingSong.Year = song.Year;
                if (!string.IsNullOrEmpty(song.Artist)) existingSong.Artist = song.Artist;
                if (!string.IsNullOrEmpty(song.Shortname)) existingSong.Shortname = song.Shortname;
                if (song.Bpm != 0) existingSong.Bpm = song.Bpm;
                if (song.Duration != 0) existingSong.Duration = song.Duration;
                if (!string.IsNullOrEmpty(song.Genre)) existingSong.Genre = song.Genre;
                if (!string.IsNullOrEmpty(song.SpotifyId)) existingSong.SpotifyId = song.SpotifyId;
                if (!string.IsNullOrEmpty(song.Album)) existingSong.Album = song.Album;
    

                SongController.Update(existingSong);
                return Accepted();
            }
            catch (Exception e)
            {
               return Conflict(e.Message);
            }
        }
      
        
        [HttpDelete]
        public IActionResult deleteSong(long id)
        {
            try
            {
                var existingSong = SongController.GetById(id);
                if (existingSong == null)
                {
                    return NotFound();
                }

                SongController.Delete(existingSong);
                return Accepted();
            }
            catch (Exception e)
            {
               return Conflict(e.Message);
            }
        }
    }
}