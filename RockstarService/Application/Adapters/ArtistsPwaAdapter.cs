using System;
using System.Collections.Generic;
using LessonCatalogService.Helpers;
using Microsoft.AspNetCore.Mvc;
using NovusLogger;
using rockstarService.Domain;

namespace rockstarService.Application.Adapters
{
    [Route("api/Artist")]
    [ApiController]
    public class ArtistPwaAdapter : ControllerBase
    {
        private readonly ArtistController ArtistController;
   

        public ArtistPwaAdapter(ArtistController ArtistController)
        {
            this.ArtistController = ArtistController;
        }

        [HttpPost]
        public ActionResult<Artist> CreateArtist(Artist song)
        {
            try{
                    ArtistController.Create(song);   
            }
            catch (Exception e)
            {
                return Conflict(e.Message);
            }
            return Accepted();
        }
        [HttpGet("GetArtistByName/", Name = "GetArtistByName")]
        public ActionResult<List<Artist>> GetArtistByName(string ArtistName)
        {
            var songs = ArtistController.GetByName(ArtistName);
            if (songs == null)
            {
                return NotFound();
            }
            return songs;
        }
        
        [HttpPost("InsertArtists/", Name = "InsertArtists")]
        public ActionResult<Artist> InsertArtists(List<Artist> artists)
        {
            foreach (var artist in artists)
            {
                if(ArtistController.GetByName(artist.Name).Count == 0){
                    ArtistController.Create(artist);
                }
            }
            return Accepted();
        }
        
        [HttpPut("{id}/updateArtist")]
        public IActionResult updateArtist(long id, Artist artist)
        {
            try
            {
                var existingArtist = ArtistController.GetById(id);
                if (existingArtist == null)
                {
                    return NotFound();
                }

                if (!string.IsNullOrEmpty(artist.Name)) existingArtist.Name = artist.Name;
            
                ArtistController.Update(existingArtist);
                
                return Accepted();
            }
            catch (Exception e)
            {
               return Conflict(e.Message);
            }
        }
      
        
        [HttpDelete]
        public IActionResult deleteArtist(long id)
        {
            try
            {
                var existingArtist = ArtistController.GetById(id);
                if (existingArtist == null)
                {
                    return NotFound();
                }

                ArtistController.Delete(existingArtist);
                return Accepted();
            }
            catch (Exception e)
            {
               return Conflict(e.Message);
            }
        }
    }
}