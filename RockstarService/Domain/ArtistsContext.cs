
using Microsoft.EntityFrameworkCore;
using NovusLogger;


namespace rockstarService.Domain
{
    public class ArtistsContext : NovusContext, INovusContext
    {
        public ArtistsContext()
        { }

        public ArtistsContext(DbContextOptions<ArtistsContext> options) : base(options)
        {
        }

        public DbSet<Song> Songs { get; set; }
        public DbSet<Artist> Artists {get; set;}

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {

            modelBuilder.ApplyConfiguration(new ProgressionRowEntityTypeConfiguration());
        }

    }
}