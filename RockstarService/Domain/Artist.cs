using System;
using System.Collections.Generic;
using System.Linq;


namespace rockstarService.Domain
{
    public class Artist
    {
        public long Id { get; set; }
        public string Name {get;set;}
        public Artist(long id, string name)
        {
            Id = id;
            Name = name;
        }
        public Artist()
        {

        }

    
    }
}
