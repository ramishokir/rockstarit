using System;
using System.Collections.Generic;
using LessonCatalogService.Helpers;

namespace rockstarService.Domain
{
    public class Song
    {
        public long Id { get; set; }
        public String Name {get;set;}
        public int Year {get;set;}
        public String Artist {get;set;}
        public String Shortname {get;set;}
        public int Bpm {get;set;}
        public int Duration {get;set;}
        public String Genre {get;set;}
        public String SpotifyId {get;set;}
        public String Album {get;set;}

      public Song(long id, string name, int year, string artist, string shortname, int bpm, int duration, string genre, String spotifyId, string album)
        {
            Id = id;
            Name = name;
            Year = year;
            Artist = artist;
            Shortname = shortname;
            Bpm = bpm;
            Duration = duration;
            Genre = genre;
            SpotifyId = spotifyId;
            Album = album;
        }

        public Song() {

        }

        public bool IsValidYear()
        {
            if(Year < 2016){
                return true;
            }
        return false;
     
        }

        public bool genreContainsMetal(){
            if(Genre.ToLower().Contains("metal")){
                return true;
            }
          return false;
        }
    }
}