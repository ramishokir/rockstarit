using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.EntityFrameworkCore;
using rockstarService.Domain.Repositories.Interfaces;

namespace rockstarService.Domain.Repositories
{
    public class ArtistRepository : IArtistRepository
    {
        private readonly ArtistsContext _context;
        internal readonly DbSet<Artist> DbSet;
        public ArtistRepository(ArtistsContext context) {
            _context = context;
            this.DbSet = context.Set<Artist>();
        }

        public Artist GetById(long id)
        {
            return DbSet.Where(Artist => Artist.Id == id).FirstOrDefault();
        }

        public void Insert(Artist Artist)
        {
            DbSet.Add(Artist);
            _context.SaveChanges();
        }

        public void Delete(Artist Artist)
        {
            DbSet.Remove(Artist);
            _context.SaveChanges();
        }

        public void Update(Artist Artist)
        {
            DbSet.Update(Artist);
            _context.SaveChanges();
        }
        public List<Artist> GetByName(string name)
        {
            return DbSet.Where(artist => artist.Name.ToLower().Contains(name.ToLower())).ToList();
    
        }

        private bool disposed = false;

        protected virtual void Dispose(bool disposing)
        {
            if (!this.disposed && disposing)
            {
                _context.Dispose();
            }
            this.disposed = true;
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        public List<Artist> GetAll()
        {
            return DbSet.ToList();
        }
    }
}