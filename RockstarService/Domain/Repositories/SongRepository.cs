using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.EntityFrameworkCore;
using NovusRabbitClient.IntegrationEvents.Models;
using rockstarService.Domain.Repositories.Interfaces;


namespace rockstarService.Domain.Repositories
{
    public class SongRepository : ISongRepository
    {
        private readonly ArtistsContext _context;
        internal readonly DbSet<Song> DbSet;
        public SongRepository(ArtistsContext context2)
        {
            _context = context2;
            this.DbSet = _context.Set<Song>();
        }

        public List<Song> GetAll()
        {
            return DbSet.ToList();
        }

        public Song GetById(long id)
        {
            return DbSet.Find(id);
        }


        public void Insert(Song Song)
        {
            DbSet.Add(Song);
            _context.SaveChanges();
        }

        public void Delete(Song Song)
        {
            DbSet.Remove(Song);
            _context.SaveChanges();
        }

        public void Update(Song Song)
        {
            DbSet.Update(Song);
            _context.SaveChanges();
        }
        public List<Song> GetByName(string songName)
        {
            return DbSet.Where(song => song.Name.ToLower().Contains(songName.ToLower())).ToList();
    
        }
        
        public List<Song> GetByGenre(string genre)
        {
            return DbSet.Where(song => song.Genre.ToLower().Contains(genre.ToLower())).ToList();
    
        }
        private bool disposed = false;

        protected virtual void Dispose(bool disposing)
        {
            if (!this.disposed && disposing)
            {
                _context.Dispose();
            }
            this.disposed = true;
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

    
    }
}