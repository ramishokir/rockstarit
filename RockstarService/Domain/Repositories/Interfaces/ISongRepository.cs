using System;
using System.Collections.Generic;
using NovusRabbitClient.IntegrationEvents.Models;

namespace rockstarService.Domain.Repositories.Interfaces
{
    public interface ISongRepository : IDisposable
    {
        List<Song> GetAll();
        Song GetById(long id);
        void Insert(Song Song);
        void Delete(Song Song);
        void Update(Song Song);
        List<Song> GetByName(string songName);
        List<Song> GetByGenre(string genre);
    }
}
