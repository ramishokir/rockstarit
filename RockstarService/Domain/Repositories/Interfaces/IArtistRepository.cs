using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;

namespace rockstarService.Domain.Repositories.Interfaces
{
    public interface IArtistRepository : IDisposable
    {
        Artist GetById(long id);
        void Insert(Artist Artist);
        void Delete(Artist Artist);
        void Update(Artist Artist);
        List<Artist> GetAll();
        List<Artist> GetByName(string songName);
    }
}
